#!/bin/bash  -x

if [ ! -d "dist/" ]; then
  mkdir dist/
fi
yarn run production
cp -rf  vendor/ dist/
cp -rf public/ dist/
cp -rf views/ dist/
cp *.php dist/
cp .htaccess dist/
cp conf.php.ini dist/conf.php
commit=`git log --format="%H" -n 1`
short=`git rev-parse --short=4 $commit`
#tar cvzf dist_$short.tar.gz dist
zip dist_$short.zip dist
result=`curl -T dist_$short.zip -k -L transfer.sh -s`
echo $result



